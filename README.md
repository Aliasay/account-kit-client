Facebook Account Kit Client
=============
Facebook Account Kit Client

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist galiasay/account-kit-client "dev-master"
```

or add

```
"galiasay/account-kit-client": "dev-master"
```

to the require section of your `composer.json` file.
